import time


class ResultsPage():
    def __init__(self, driver):
        self.driver = driver
        self.xpath_item_pagina_resultados_1 = "//ul[@class='result-items']//li[1]"
        self.xpath_item_pagina_resultados_2 = "//ul[@class='result-items']//li[2]"
        self.xpath_item_pagina_resultados_3 = "//ul[@class='result-items']//li[3]"
        self.xpath_selecciona_item_1 = "//li[1]//a[1]//div[1]//img[1]"
        self.xpath_selecciona_item_2 = "//li[2]//a[1]//div[1]//img[1]"
        self.xpath_selecciona_item_3 = "//li[3]//a[1]//div[1]//img[1]"

    def imprimir_resultados(self):
        Lista_Resultados = self.driver.find_element_by_xpath(self.xpath_item_pagina_resultados_1).text
        Lista_Resultados2 = self.driver.find_element_by_xpath(self.xpath_item_pagina_resultados_2).text
        Lista_Resultados3 = self.driver.find_element_by_xpath(self.xpath_item_pagina_resultados_3).text
        print("Opción 1")
        print(Lista_Resultados)
        print("Opción 2")
        print(Lista_Resultados2)
        print("Opción 3")
        print(Lista_Resultados3)
        time.sleep(5)

    def elegir_cancion(self, elijo):
        if elijo == 1:
            print("Opcion1")
            self.driver.find_element_by_xpath(self.xpath_selecciona_item_1).click()
            time.sleep(10)

        elif elijo == 2:
            print("Opcion2")
            self.driver.find_element_by_xpath(self.xpath_selecciona_item_2).click()
            time.sleep(10)

        elif elijo == 3:
            print("Opcion3")
            self.driver.find_element_by_xpath(self.xpath_selecciona_item_3).click()
            time.sleep(10)

        else:
            print("error")


        time.sleep(10)

