import time


class HomePage():
    def __init__(self, driver):
        self.driver = driver
        self.id_barra_buscar = "q"
        self.boton_buscar_xpath = "//button[contains(text(),'Search')]//*[local-name()='svg']"

    def ingresar_cancion(self, busqueda):
        self.driver.find_element_by_name(self.id_barra_buscar).send_keys(busqueda)
        time.sleep(5)

    def click_buscar(self):
        self.driver.find_element_by_xpath(self.boton_buscar_xpath).click()
        time.sleep(5)
