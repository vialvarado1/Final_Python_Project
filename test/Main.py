
from selenium import webdriver
import time
import unittest


from Pages.homepage import HomePage
from Pages.resultspage import ResultsPage
from Pages.reproductorpage import ReproductorPage


class MainTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = webdriver.Chrome("C:\WebDriver\chromedriver.exe")
        # driver = webdriver.Firefox()
        # driver = webdriver.Ie()

        cls.driver.maximize_window()
        time.sleep(5)

    def test_busqueda(self):
        driver = self.driver
        driver.get("https://bandcamp.com/")
        Main=HomePage(driver)
        busqueda= input("¿Qué canción quieres buscar?")
        Main.ingresar_cancion(busqueda)
        Main.click_buscar()
        self.assertAlmostEqual(busqueda, "eminem")

    def test_pagina_resultados(self):
        driver = self.driver
        Main2= ResultsPage(driver)
        Main2.imprimir_resultados()
        elijo= int(input("¿Cual quieres reproducir? Escribe un número del 1-3"))
        Main2.elegir_cancion(elijo)

        self.assertNotEqual(elijo, [4, 5, 6, 7, 9, 10])

    def test_reproductor_page(self):
        driver = self.driver
        Main3= ReproductorPage(driver)
        Main3.prueba_reproductor()



    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.close()
        cls.driver.quit()
        print("test done")


if __name__ == '__main__':
    unittest.main()
